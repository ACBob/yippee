import sys
import os

import feeds, config
import logging, logging.handlers

from discord.ext import tasks
import discord

import argparse

import json


class YippeeClient(discord.Client):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.load_config()

    def load_config(self):
        # Do arg parsing
        parser = argparse.ArgumentParser(
            "yippee",
            description="A wee discord bot for the seemingly simple task of YouTube uploads and Twitch streams.",
        )

        parser.add_argument(
            "--tokens",
            help="The path to the tokens.txt file.",
            type=str,
            default="./tokens.txt",
        )
        parser.add_argument(
            "--config",
            help="The path to the config.json5 file.",
            type=str,
            default="./config.json5",
        )
        parser.add_argument(
            "--debug",
            help="Specifies the debug mode for the bot. DO NOT RUN THIS IN PRODUCTION.",
            type=bool,
            default=False,
        )
        parser.add_argument(
            "--amnesia",
            help="If specified, the bot will not use the cache file.",
            type=bool,
            default=False,
        )
        parser.add_argument(
            "--mute",
            help="If specified, the bot will NEVER inform of new posts.",
            type=bool,
            default=False,
        )

        self.args = parser.parse_args()

        # Set debug mode first and foremost
        self.debug = self.args.debug

        self.logger = logging.getLogger("Yippee")

        if self.debug:
            self.logger.warning("DEBUG MODE IS ENABLED.")
            self.logger.setLevel(logging.DEBUG)

        # Load the tokens from the tokens.txt file
        # First line is the discord bot token
        # The second/third line is the Twitch nonsense
        if not os.path.exists(self.args.tokens):
            self.logger.error("Given token path (%s) does not exist.", self.args.tokens)
            sys.exit(1)

        with open(self.args.tokens, "r") as f:
            self.tokens = f.read().splitlines()
            self.tokens = [token.strip() for token in self.tokens]

        # Load the config file
        self.config = config.YippeeConfig()

        if not self.config.load(self.args.config):
            self.logger.error("Could not load config file, sad!")
            sys.exit(1)

        # If we're amnesiac, we don't store or load memory.
        self.amnesiac = (
            self.args.amnesia or len(self.config.get("bot", "memory", default="")) < 1
        )

        if self.amnesiac:
            self.logger.info("Bot is an amnesiac; will not store or load cache.")

        # Setup logging file
        if self.config.get("bot", "log"):
            self.log_file_handler = logging.handlers.RotatingFileHandler(
                filename=self.config.get("bot", "log", "path", default="./yippee.log"),
                encoding="utf-8",
                maxBytes=self.config.get(
                    "bot", "log", "size", default=32 * 1024 * 1024
                ),
                backupCount=self.config.get("bot", "log", "rotation"),
            )

            dateFormat = "%Y-%m-%d %H:%M:%S"
            fileFormat = logging.Formatter(
                "[{asctime}] [{levelname:<8}] {name}: {message}", dateFormat, style="{"
            )
            self.log_file_handler.setFormatter(fileFormat)

            self.logger.addHandler(self.log_file_handler)
            # HACK: add the handler to discord's logging...
            logging.getLogger("discord").addHandler(self.log_file_handler)

        self.feeds = []

        self.youtube = None
        if self.config.get("platforms", "youtube"):
            if self.config.get("platforms", "youtube", "update_rate", default=0) > 0:
                self.logger.info("YouTube enabled!")
                self.youtube = feeds.YippeeYoutubeFeed(self.config)
                self.feeds.append(self.youtube)
            else:
                self.logger.info("YouTube Disabled.")
        else:
            self.logger.info("YouTube Disabled.")

        self.twitch = None
        if self.config.get("platforms", "twitch"):
            if self.config.get("platforms", "twitch", "update_rate", default=0) > 0:
                self.logger.info("Twitch enabled!")
                if len(self.tokens) >= 3:
                    self.twitch = feeds.YippeeTwitchFeed(self.config)
                    self.twitch.twitch_client = self.tokens[1]
                    self.twitch.twitch_secret = self.tokens[2]
                    self.feeds.append(self.twitch)
                else:
                    self.logger.warning(
                        "Twitch is enabled BUT there is no twitch tokens! Disabling Twitch..."
                    )
            else:
                self.logger.info("Twitch Disabled.")
        else:
            self.logger.info("Twitch Disabled.")

        # zero-out feeds so we update immediately upon first tick
        for feed in self.feeds:
            feed.update_tick = 0

        # Let's load our cache.
        self.remember()

    def remember(self):
        # We can't remember if we're an amnesiac.
        if self.amnesiac:
            return

        # If there's no memory, don't bother
        if not os.path.exists(self.config.get("bot", "memory")):
            return

        with open(self.config.get("bot", "memory"), "r") as f:
            memory = json.load(f)

            for feed in self.feeds:
                if feed.platform in memory:
                    self.logger.debug(f"Remembered {feed.platform}")
                    feed.cache = memory[feed.platform]

            # Try and remember the twitch API key
            if self.twitch:
                self.twitch.twitch_api_key = memory.get("twitch_api_token", "")

    def memorize(self):
        # We can't commit to memory if we're an amnesiac.
        if self.amnesiac:
            return

        with open(self.config.get("bot", "memory"), "w") as f:
            cache = {}
            for feed in self.feeds:
                cache[feed.platform] = feed.cache

            # Store the twitch API token so we don't need to try to get it later.
            if self.twitch:
                cache["twitch_api_token"] = self.twitch.twitch_api_key

            json.dump(cache, f)

    @tasks.loop(seconds=60)
    async def update_cycle(self):
        self.logger.debug("Update Cycle...")

        # Feed updating
        for feed in self.feeds:
            self.logger.debug(f"Ticking {feed.platform}")

            if feed.update_tick > 0:
                feed.update_tick -= 1
            else:
                feed.update_tick = feed.update_rate

                self.logger.debug(f"Updating {feed.platform}")

                feed.update()

                if not self.args.mute:
                    await feed.inform(self)

        # Once we've gone across the cache, save it
        self.memorize()

    @update_cycle.before_loop
    async def before_update(self):
        self.logger.debug("Waiting until ready before updating...")
        await self.wait_until_ready()

    async def setup_hook(self) -> None:
        self.logger.info("Hello Hook!")

        if not self.update_cycle.is_running():
            self.update_cycle.start()

        return await super().setup_hook()

    async def on_ready(self):
        self.logger.info("Bot readied!")


if __name__ == "__main__":
    # Setup logging
    discord.utils.setup_logging()
    client = YippeeClient(intents=discord.Intents.default())

    try:
        client.run(
            client.tokens[0], log_handler=None
        )  # HACK: if we specify it as the client log file, it adds obtuse formatting.
    except discord.errors.LoginFailure as e:
        client.logger.error("Discord login failure: %s", e)
