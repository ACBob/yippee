# YIPPEE
A wee (<500 LOC) MIT-licensed Python-based Discord bot for notifying of new uploads from YouTube and Twitch streams starting.
After it was decided to be used as a replacement for something serious, it has been growing a bit to be cleaner.

## Usage
This bot is designed such that one server may have a bot for themselves, a very self-hosted approach.

 * Install a recent version of python (I have tested 3.6+ and 3.10+)
 * Install requirements.txt (`pip3 install -r requirements.txt`)
 * Dump tokens and such in a `tokens.txt` next to the config.
 * Configure the bot (plentiful comments)
 * Run the bot!

There's a lot of description dumped away in the config file to help explain matters.