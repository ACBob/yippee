import json5

# import logging


class YippeeConfig:
    """
    A simple helper class for interacting with the multi-layered config file.

    Example usage:
    ```
    config = YippeeConfig()
    config.load('config.json5')

    # Access a setting value
    api_key = config.get('api', 'key', default='my_default_key')
    ```
    """

    def __init__(self):
        # Holds the config dict
        self._conf = {}

        # self.logger = logging.getLogger("Yippee").getChild("Config")

    def load(self, path: str) -> bool:
        """
        Loads a JSON5 file from the given path and stores it in the config object.

        :param path: The path to the JSON5 file to load.
        :return: True if the file was loaded successfully, False otherwise.
        """
        with open(path, "r") as f:
            self._conf = json5.load(f)

        return True

    def get(self, *keys, default=None):
        """
        Returns the value under the specified path from the loaded config file, or the default value if the path does not exist.

        :param keys: A sequence of keys representing the path to the desired value.
        :param default: The default value to return if the path does not exist (default: None).
        :return: The value under the specified path, or the default value.
        """
        cur = self._conf

        # print(f"GRABBING {'/'.join(keys)}")

        for key in keys:
            try:
                cur = cur[key]
            except (KeyError, TypeError):
                # print("failed to slurp", key)
                return default

        return cur
