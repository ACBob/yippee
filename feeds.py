import untangle
import requests

from config import YippeeConfig

import discord

import logging


class YippeeFeed:
    """
    A base class for managing and updating feeds from various platforms.

    NOTE: To this class, a feed is a platform that has mutliple sources.
    """

    # The config path to slurp from
    platform: str = "?"

    # Config instance we're slurping from
    config: YippeeConfig = None

    # How often we should update
    update_rate: int = 15

    # Our current update tick
    update_tick: int = 15

    def __init__(self, conf: YippeeConfig):
        self.config = conf
        self.logger = (
            logging.getLogger("Yippee").getChild("Feed").getChild(self.platform)
        )

        # This is our cache.
        self.cache = {}

        # Channels to send to.
        self.discord_channels = []

        # The places we slurp from
        self.feeds = {}

        self.load()

    def load(self):
        """
        (Re)Loads the feed config from the config file.
        """

        self.discord_channels = self.config.get(
            "platforms", self.platform, "channels", default={}
        ).keys()

        self.update_rate = self.update_tick = self.config.get(
            "platforms", self.platform, "update_rate", default=15
        )

        self.feeds = self.config.get("platforms", self.platform, "feeds", default=[])

    def update(self):
        """
        Stub method for specific feeds to overload.
        """
        pass

    async def inform(self, client: discord.Client):
        """
        Stub method for specific feeds to overload.
        """
        pass

    def _get_channel_overrideable(self, key, feedid, dscid):
        """
        A channel overrideable here is any attribute that should be attached to a channel of some kind, like message or colour

        This follows a hierarchy that bubbles from local channels, up to platform channels, up to global channels.
        """
        # NOTE: This function can probably be implemented more efficiently. Too bad!
        return (
            self.config.get(
                "platforms",
                self.platform,
                "feeds",
                feedid,
                "channels",
                str(dscid),
                key,
            )
            or self.config.get(
                "platforms",
                self.platform,
                "channels",
                str(dscid),
                key,
            )
            or self.config.get(
                "platforms",
                self.platform,
                "feeds",
                feedid,
                key,
            )
        )

    def _get_channels(self, feedid):
        return (
            self.config.get(
                "platforms",
                self.platform,
                "feeds",
                feedid,
                "channels",
                default={},
            ).keys()
            or self.config.get(
                "platforms",
                self.platform,
                "channels",
                default={},
            ).keys()
            or self.discord_channels
        )

    def clear_cache(self):
        self.cache.clear()


class YippeeYoutubeFeed(YippeeFeed):
    """
    Youtube Feed implementation.
    """

    platform = "youtube"

    # The message format to use when writing to channels.
    message = "@everyone, {author} just uploaded **{title}** to YouTube!\n{url}"

    def load(self):
        super().load()

        # Slurp out message.
        self.message = self.config.get(
            "platforms", self.platform, "message", default=self.message
        )

    def update(self):
        for ytid in self.feeds.keys():
            # ytchan = self.feeds[ytid]

            # If there's an error, we want to treat it largely as it never happened.
            if ytid in self.cache and "error" in self.cache[ytid]:
                self.cache.pop(ytid)

            self.logger.debug(f"Trying to get {ytid}...")

            response = requests.get(
                f"https://www.youtube.com/feeds/videos.xml?channel_id={ytid}"
            )

            if response.status_code != 200:
                self.logger.error(f"Failed to get {ytid} ({response.status_code})")
                self.cache[ytid] = {
                    "error": response.status_code,
                }
                continue

            # Parse the RSS feed
            tree = untangle.parse(response.text).feed

            # <title> in root is channel name
            author = tree.title.cdata

            # The first <link> is a link back to this RSS feed
            # The second <link> is a link to the channel
            author_url = tree.link[1].cdata

            # First entry should be the most recent video
            # If there's no videos, there won't be any entry elements.
            video = tree.get_elements("entry")
            video = video[0] if len(video) > 0 else video
            if video:
                # Pretty self-explanatory...
                title = video.title.cdata
                url = video.link["href"]

                # The RSS feed has a lot of extra information in the <media:group> tag.
                media = video.media_group
                thumbnail_url = media.media_thumbnail["url"]
                description = media.media_description.cdata

                # If we've not cached it, or the URL is different than last time, update the entry.
                # TODO: This will falsely trigger on video deletion, we need to compare times!
                # NOTE: Would it be interesting to delete the message of a deleted video automagically...?
                if (not ytid in self.cache) or self.cache[ytid]["url"] != url:
                    self.logger.debug(f"Found NEW video {title}")

                    self.cache[ytid] = {
                        "title": title,
                        "url": url,
                        "author": author,
                        "author_url": author_url,
                        "description": description,
                        "thumbnail_url": thumbnail_url,
                        # We pretend we've informed if we're adding this as a base state.
                        "informed": not ytid in self.cache,
                        # "informed": False,
                    }
                else:
                    self.logger.debug("No new videos!")
            else:
                self.logger.debug("No videos?")

    async def inform(self, client: discord.Client):
        for ytid in self.cache.keys():
            video = self.cache[ytid]

            # If we've already informed, we don't want to again.
            # Unless we're in debug, in which case, we ignore that :3
            # We ignore errors forever though.
            if "error" in video or (video["informed"] and not client.debug):
                continue
            video["informed"] = True  # Mark this informed now!

            for dscid in self._get_channels(ytid):
                msgfmt = (
                    self._get_channel_overrideable("message", ytid, dscid)
                    or self.message
                )

                channel = client.get_channel(int(dscid))
                if channel:
                    await channel.send(msgfmt.format(**video))
                else:
                    self.logger.error(f"Failed to find Discord Channel '{dscid}'.")


# TODO: Youtube Music!
# NOTE: Largely the same as YouTube I imagine, but handles for playlists instead...
# class YippeeYTMusicFeed(YippeeYoutubeFeed):
#     platform = "ytmusic"


class YippeeTwitchFeed(YippeeFeed):
    """
    Twitch Feed implementation.
    """

    platform = "twitch"

    # The color to use when embedding.
    color = "#6441A4"

    # The message format to use when writing to channels.
    message = "@everyone, {author} just went live!"

    # Client Secret / Token
    twitch_client = ""
    twitch_secret = ""

    # API Key we get from secret & token
    twitch_api_key = ""

    def load(self):
        super().load()

        # Slurp out color.
        self.color = self.config.get(
            "platforms", self.platform, "color", default=self.color
        )

        # Slurp out message.
        self.message = self.config.get(
            "platforms", self.platform, "message", default=self.message
        )

    def _grab_api_key(self):
        # Twitch is really annoying. Your app, armed with ID & secret, must send a request to then get a key to use for further requests.
        # truely a bruh moment.
        self.logger.info("(Re)getting a twitch token...")

        response = requests.post(
            "https://id.twitch.tv/oauth2/token",
            data={
                "client_id": self.twitch_client,
                "client_secret": self.twitch_secret,
                "grant_type": "client_credentials",
            },
        )

        if response.status_code != 200:
            self.logger.error(
                "Couldn't retrieve Twitch token: (%d) %s",
                response.status_code,
                response.json()["message"],
            )
            self.twitch_api_key = ""
            return

        # Otherwise...
        self.twitch_api_key = response.json()["access_token"]

    def update(self):
        # We want to get the API key then.
        if not self.twitch_api_key:
            self._grab_api_key()

        for twid in self.feeds.keys():
            # twchan = self.feeds[twid]
            self.logger.debug(f"Trying to get {twid}...")

            # If there's an error, we want to treat it largely as it never happened.
            if twid in self.cache and "error" in self.cache[twid]:
                self.cache.pop(twid)

            URL = f"https://api.twitch.tv/helix/streams?user_login={twid}&first=1"
            response = requests.get(
                URL,
                headers={
                    "Client-Id": self.twitch_client,
                    "Authorization": f"Bearer {self.twitch_api_key}",
                },
            )

            if response.status_code != 200:
                # HACK: if it's a 401, and our message is "Invalid OAuth token", try and get a new token.
                if (
                    response.status_code == 401
                    and response.json()["message"] == "Invalid OAuth token"
                ):
                    # Try again.
                    self.logger.error(
                        "Couldn't retrieve twitch - invalid token, trying to get a new one and attempting again..."
                    )
                    self._grab_api_key()
                    self.update_twitch()
                    return
                else:
                    self.logger.error(
                        f"Failed to get {twid} ({response.status_code}): {response.json()['text']}"
                    )
                    self.cache[twid] = {
                        "error": response.status_code,
                    }
                    continue

            data = response.json()

            # If there's no data, they're not streaming. Remove them from the cache!
            if not data["data"]:
                if twid in self.cache:
                    self.cache.pop(twid)
                continue

            stream = data["data"][0]
            title = stream["title"]
            url = f"https://www.twitch.tv/{twid}"

            # If we've not cached it, or the URL has changed, update the entry.
            update_this = (not twid in self.cache) or self.cache[twid]["title"] != title

            if update_this:
                self.cache[twid] = {
                    "title": title,
                    "url": url,
                    "game": stream["game_name"],
                    "viewers": stream["viewer_count"],
                    "mature": stream["is_mature"],
                    "thumbnail_url": stream["thumbnail_url"],
                    "author": stream["user_name"],
                    "author_url": f"https://twitch.tv/{twid}",  # We can cheat this because it's so simple
                    # We always treat streams as uninformed, as we'll only add it to the cache if they are streaming.
                    # The base state is to assume that they're not streaming, and if otherwise happens, to treat it as if they've started.
                    "informed": False,
                    "messages": self.cache.get(twid, {}).get("messages", {}),
                }

    async def inform(self, client: discord.Client):
        for twid in self.cache.keys():
            # We ignore the key "twitch_api_token" for obvious reasons.
            if twid == "twitch_api_token":
                continue

            stream = self.cache[twid]

            # If we've already informed, we don't want to again.
            # Unless we're in debug, in which case, we ignore that :3
            # We ignore errors forever though.
            if "error" in stream or (stream["informed"] and not client.debug):
                continue
            stream["informed"] = True  # Mark this informed now!

            self.logger.debug(f"Informing {twid}")

            for dscid in self._get_channels(twid):
                self.logger.debug(f"Informing {twid} in {dscid}...")
                color = (
                    self._get_channel_overrideable("color", twid, dscid) or self.color
                )

                color = discord.Colour.from_str(str(color))

                message = (
                    self._get_channel_overrideable("message", twid, dscid)
                    or self.message
                )

                channel = client.get_channel(int(dscid))
                if channel:
                    # Let's build our embed.
                    embed = (
                        discord.Embed(
                            title=stream["title"], url=stream["url"], color=color
                        )
                        .set_author(name=stream["author"], url=stream["url"])
                        .set_thumbnail(
                            url=stream["thumbnail_url"].format(
                                width="320", height="180"
                            )
                        )
                        .add_field(name="Game", value=stream["game"], inline=True)
                        .add_field(name="Viewers", value=stream["viewers"], inline=True)
                    )

                    if stream["mature"]:
                        embed.add_field(name="Mature", value="🔞", inline=True)

                    if str(dscid) in stream["messages"]:
                        self.logger.debug(
                            f"Trying to edit old {twid} message ({stream['messages'][dscid]})"
                        )

                        try:
                            discmsg = await channel.fetch_message(
                                stream["messages"][str(dscid)]
                            )
                            await discmsg.edit(
                                content=message.format(**stream), embed=embed
                            )
                            continue
                        except discord.errors.NotFound:
                            # If we can't get it, fall-through and post it
                            discmsg = None
                            self.logger.debug(
                                f"Couldn't get {twid}'s old message in {dscid}, sending a new one..."
                            )

                    message = await channel.send(message.format(**stream), embed=embed)
                    stream["messages"][str(dscid)] = message.id

                else:
                    self.logger.error(f"Failed to find Discord Channel '{dscid}'.")
